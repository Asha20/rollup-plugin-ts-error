/*
 * Modifying this file while Rollup is in watch mode results in
 * a "TypeError: Cannot read property 'fileName' of undefined".
 **/

export function assert(condition: any, message: string) {
  if (!condition) {
    throw new Error(message);
  }
}
