import ts from "@wessberg/rollup-plugin-ts";

export default {
  input: "src/main.ts",
  output: {
    name: "Foo",
    file: "dist/output.js",
    format: "iife",
    sourcemap: true,
  },
  plugins: [ts()],
};
